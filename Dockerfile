FROM ruby:latest

# Installing NodeJS
RUN apt-get update \
    && apt-get install -y --no-install-recommends
RUN wget http://nodejs.org/dist/v6.9.1/node-v6.9.1-linux-x64.tar.gz -O - | tar zxv -C /usr/local --strip-components=1 
